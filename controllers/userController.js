const jwt = require('jsonwebtoken')
const HttpStatus = require('http-status-codes')
const User = require('../models/Users')
const utils = require('../utils/generateAccNumber');

module.exports = {
  // CREATE USER
  createUser: async (req, res, next) => {
    try {
      const { userName, name, password } = req.body;
      const accNumber = await utils.generateAccNumber()
      const user = new User({
        userName: userName,
        name: name,
        password: password,
        accNumber: accNumber
      })
      await user.save()
      return res.status(HttpStatus.CREATED).json({ userName, name, accNumber })
    } catch (error) {
      return res.status(HttpStatus.NOT_ACCEPTABLE).json({ error: error })
    }
  },

  // Authenticate user 
   authenticateUser: async (req, res, next) => {
    const { userName, password } = req.body
    try {
      const user = await User.findByCredentials(userName, password)
      const token = await user.generateAuthToken(user)
      const { accNumber, name } = user
      return res.send({ accNumber, name, userName })
    } catch (error) {
      return res.status(HttpStatus.BAD_REQUEST).json({ error: error.message })
    }
  },

  uploadCSV: async (req, res, next) => {
    try {
      // mongoimport -u "username" -p "password" -d "test" -c "collections" --type csv --file myCsv.csv --headerline

    } catch(error) {
      return res.status(HttpStatus.BAD_REQUEST).json({ error: error})
    }
  }
}