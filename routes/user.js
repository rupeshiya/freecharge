const express = require('express')
const router = express.Router()
const userController = require('../controllers/userController')

// user login
router.post(
  '/login',
  userController.authenticateUser
)

// register user 
router.post(
  '/register',
  userController.createUser
)

module.exports = router
