module.exports = {
  generateAccNumber: async () => {
    const accNo = Math.random().toString(36).substr(2, 9);
    console.log('accNumber ', accNo)
    return accNo;
  }
}