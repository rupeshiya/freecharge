const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const saltRounds = 8
const Schema = mongoose.Schema

const UserSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  userName: {
    type: String,
    required: true
  },
  accNumber: {
    type: String
  },
  password: {
    type: String,
    required: true
  },
  token: {
    type: String
  },
  createdAt: {
    type: Date,
    default: Date.now()
  },
  updatedAt: {
    type: Date,
    default: Date.now()
  }
})

// generate auth token
// Schema Methods, needs to be invoked by an instance of a Mongoose document
UserSchema.methods.generateAuthToken = async (user) => {
  const token = jwt.sign({ _id: user._id.toString() }, process.env.JWT_SECRET);
  user.token = token;
  await user.save();
  return token;
}

// Schema Statics are methods that can be invoked directly by a Model
UserSchema.statics.findByCredentials = async (userName, password) => {
  const user = await User.findOne({ userName })

  if (!user) {
    throw new Error('Please upload the CSV details to register an account!')
  } else {
    const isMatch = await bcrypt.compare(password, user.password)
    if (!isMatch) {
      throw new Error('Incorrect password provided')
    } else {
      return user
    }
  }
}

// hash user password before saving into database
UserSchema.pre('save', async function (next) {
  const user = this

  if (user.isModified('password')) {
    user.password = await bcrypt.hash(user.password, saltRounds)
  }

  next()
})

const User = mongoose.model('User', UserSchema)
module.exports = User
