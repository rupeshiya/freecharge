require('dotenv').config()
require('./config/mongoose')
const express = require('express')
const morgan = require('morgan')
const cookieParser = require('cookie-parser')
const path = require('path')
const bodyParser = require('body-parser')
const cors = require('cors')
const helmet = require('helmet')
const hpp = require('hpp')
var winston = require('./config/winston')
const fileConstants = require('./config/fileHandlingConstants')
const userRouter = require('./routes/user')
const createError = require('http-errors')
const PORT = process.env.PORT || 5000;


const app = express()
app.use(cors())

app.use(bodyParser.json())
app.use(cookieParser())

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

morgan.token('data', (req, res) => {
  return JSON.stringify(req.body)
})

app.use(
  morgan(
    ':remote-addr - :remote-user [:date[clf]] ":method :url" :status :res[content-length] ":referrer" ":user-agent" :data',
    { stream: winston.stream }
  )
)

app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))


// TO PREVENT XSS ATTACK
app.use(helmet())

// TO PREVENT CLICK JACKING
app.use((req, res, next) => {
  res.append('X-Frame-Options', 'Deny')
  res.set('Content-Security-Policy', "frame-ancestors 'none';")
  next()
})

// TO PREVENT THE QUERY PARAMETER POLLUTION
app.use(hpp())

app.use('/user', userRouter)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404, "route doesn't exist"))
})

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // To include winston logging (Error)
  winston.error(
    `${err.status || 500} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip} - ${req.body}`
  )

  // render the error page
  res.status(err.status || 500)
  res.render('error')
  next()
})

app.listen(PORT, () => {
  console.log(`[+] listening on port: ${PORT}`)
})
