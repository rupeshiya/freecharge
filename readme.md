
## Prerequisite: 

These are the requirement that should be installed locally on your machine.

- Node.js
- MongoDB


## How to setup node.js on your machine?

- Move to: [link](https://nodejs.org/en/download/) choose the operating system as per your machine and start downloading and setup by clicking recommended settings in the installation wizard.

## How to setup MongoDB on your machine?

- Move to: [link](https://docs.mongodb.com/manual/administration/install-community/) look at the left sidebar and choose the operating system according to your machine. Then follow the steps recommended in the official docs.

```
Note: You can also use the MongoDB servers like Mlab or MongoDB Cluster server
```

## How to set up this project locally?
- Install all the dependencies using:
```sh
npm install 
```
- Run the development server using:
```sh
npm run dev
```
- Now the server is running on PORT 5000 or the PORT mentioned in the environment **.env** variables

```
Note: Setup the environment variables as mentioned below
```

## What are the environment variables required to run the project locally on a machine?
- Follow these steps to set the environment variable for development:
- Move to the root directory of the project and open **.env** (for development) 
- PORT = 5000
- NODE_ENV = "development"
- JWT_SECRET="<YOUR SECRET KEY>"
- DATABASE_URL="<YOUR DB URL>"

## Check logs:
```
 Open /logs folder in root directory
```

## Allowed HTTPs requests:
<pre>
POST    : To register user (/user/register)
POST    : To login user (/user/login)
POST    : To upload CSV File (/user/upload)
</pre>

## Screenshots: 

### Register to create account: 
![register](https://user-images.githubusercontent.com/31209617/96041958-458bd600-0e8a-11eb-9885-ab1b4a0bc90b.PNG)


### Login 
![loginAccExists](https://user-images.githubusercontent.com/31209617/96042016-60f6e100-0e8a-11eb-9c18-a8aa037e9bfa.PNG)


### Upload (Not done completely)



#### Made with :heart: by [Rupeshiya](https://github.com/Rupeshiya)